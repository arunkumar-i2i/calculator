var result = document.calculator.result;
var accumulate = 0;
var newNumberFlag = false;
var selectOperator = "";
var display = "";
//Function for print the number input
function numberPressed (number) {
    if (newNumberFlag) {
        result.value  = number;
        newNumberFlag = false;
    } else {
        if (result.value == "0")
            result.value = number;
        else
            result.value += number;
    }
}

//Function for calculator operation
function Operation (Operator) {
    var resultValue = result.value;
    if (newNumberFlag && selectOperator != "=");
    else {
        newNumberFlag = true;
        if ( '+' == selectOperator )
            accumulate += parseFloat(resultValue);
        else if ( '-' == selectOperator )
            accumulate -= parseFloat(resultValue);
        else if ( '/' == selectOperator )
            accumulate /= parseFloat(resultValue);
        else if ( '*' == selectOperator )
            accumulate *= parseFloat(resultValue);
        else {
            accumulate = parseFloat(resultValue);
        }
        result.value = accumulate;
        selectOperator = Operator;
    }
}

//Function for Decimal value
function Decimal () {
    var currentResult = result.value;
    if (newNumberFlag) {
        currentResult = "0.";
        newNumberFlag = false;
    } else {
        currentResult += ".";
    }
    result.value = currentResult;
}

//Function for negative value
function negative () {
    result.value = parseFloat(result.value) * -1;
}

//Function for clear input entries
function Clear () {
    accumulate = 0;
    selectOperator = "";
    result.value = "0";
    newNumberFlag = true;
}
